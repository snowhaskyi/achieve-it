//
//  FirstQuestionViewController.swift
//  Achieve_IT

import UIKit

final class FirstQuestionViewController: UIViewController, UITextViewDelegate {
    
    let placeholder = "Give an example of your personal dream"

    let label = UILabel()
    let textView = UITextView()
    let button = CustomContinueButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.hidesBarsOnSwipe = false
        self.textView.delegate = self
        view.backgroundColor = .primaryBackground
            
        let items = [label, textView, button]
        for i in items {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        setup()
        constraints()
        backBarButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        textView.becomeFirstResponder()
    }
    
    @objc func continueButtonAction() {
        let check = placeholder
        if textView.text.isEmpty  == false && textView.text != check {
            ClipBoard.clipBoard.name = textView.text
            let newVC = SecondQuestionViewController()
            navigationController?.pushViewController(newVC, animated: true)
        }
    }

    func setup(){
        label.textColor = .white
        label.text = "Name of your dream"
        label.font = .H2
        label.lineMode()

        textView.text = placeholder
        textView.textColor = .secondaryText
        textView.lineMode()
        textView.font = .H6
        textView.backgroundColor = .primaryBackground
        let newPosition = textView.beginningOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)

        button.setBackgroundColor(.activeElementBackground, for: .normal)
        button.setBackgroundColor(.disabledElementBackground, for: .disabled)
        button.isEnabled = false
        button.layer.cornerRadius = 22
        button.setTitle("Continue", for: .normal)
        button.addTarget(self, action: #selector(continueButtonAction), for: .touchUpInside)
        button.setTitleColor(.activeElementText, for: .normal)
        button.titleLabel?.font = .A1
    }
    
    func constraints(){
        let buttonHeight:CGFloat = 48
        
        NSLayoutConstraint.activate([
            
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 108),
            
            textView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 28),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            textView.bottomAnchor.constraint(equalTo: button.topAnchor, constant: -10),
            
            button.heightAnchor.constraint(equalToConstant: buttonHeight),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 44),
            view.keyboardLayoutGuide.topAnchor.constraint(equalToSystemSpacingBelow: button.bottomAnchor, multiplier: 2.0),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -44),
        ])
    }
    
//MARK: - textView's placeholder
    
     func textViewDidChange(_ textView: UITextView) {
        button.isEnabled = !textView.text.isEmpty
         if textView.text.isEmpty {
             textView.text = placeholder
             let newPosition = textView.beginningOfDocument
             textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
         }
    }
    
     func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
        }
    }

     func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         
         if textView.text == placeholder {
             textView.text = ""
         }
         return true
    }
}
