//
//  GoalCard.swift
//  Achieve_IT

import UIKit

final class GoalCard: UIViewController,
                     UITableViewDelegate,
                     UITableViewDataSource,
                     UIImagePickerControllerDelegate & UINavigationControllerDelegate,
                     ReloadTableViewDelegate,
                     ChangeStateOFinalList,
                     СhangeEditState,
                     CallImagePickerDelegate,
                     ReloadToDoListDelegate {
   
   var addState: stateAddButton = .hidden
   var toDoneArray:[Int] = []
   var editIs:editStateOfTopCell = .off
   var progressState:whatList = .willDoList
   
   let tableView = UITableView()
   let add = UIButton()

   override func viewDidAppear(_ animated: Bool) {
       if progressState == .willDoList {
           sleep(1)
           scrollToBottom()
       }
   }
   
   override func viewDidLoad() {
       super.viewDidLoad()
       navigationController?.hidesBarsOnSwipe = true

       tableView.reloadData()
       tableView.frame = CGRect(x: 0, y: 0,
                                width: view.frame.size.width,
                                height: view.frame.size.height)
       tableView.separatorColor = .clear
               
       view.addSubview(tableView)
       view.addSubview(add)
       add.translatesAutoresizingMaskIntoConstraints = false
                       
       backBarButton()
       createGoalRightBarButton()
       addButton()
       
       tableView.backgroundColor = .primaryBackground
       tableView.register(TopCell.self, forCellReuseIdentifier:  TopCell.identifier)
       tableView.register(SwitchCell.self, forCellReuseIdentifier:  SwitchCell.identifier)
       tableView.register(WillDoCell.self, forCellReuseIdentifier: WillDoCell.identifier)
       tableView.register(AddCell.self, forCellReuseIdentifier: AddCell.identifier)
       tableView.register(DoneCell.self, forCellReuseIdentifier: DoneCell.identifier)
       tableView.delegate = self
       tableView.dataSource = self
   }
   // MARK: Custom elements
   func createGoalRightBarButton() {
       let button = UIButton(type: .custom)
       button.setTitleColor(.black, for: .normal)
       button.frame = CGRect(x: 0, y: 0, width: 32, height: 19)
       button.setTitle("Create", for: .normal)
       button.titleLabel?.font = .A1
       button.backgroundColor = .activeElementBackground
       button.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
       button.layer.cornerRadius = 22
       
       let menuBarItem = UIBarButtonItem(customView: button)
       self.navigationItem.rightBarButtonItem = menuBarItem
       button.addTarget(self, action: #selector(toMainView), for: .touchUpInside)
   }

   func addButton() {
       add.backgroundColor = .activeElementBackground
       add.setTitle("Done", for: .normal)
       add.setTitleColor(.secondaryText, for: .normal)
       add.titleLabel?.font = .A1
       add.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
       add.layer.cornerRadius = 10
       add.addTarget(self, action: #selector(addToDoneList), for: .touchUpInside)
       
       if addState == .hidden {
           add.isHidden = true
       } else {
           add.isHidden = false
       }
       
       NSLayoutConstraint.activate([
           add.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: -20),
           add.bottomAnchor.constraint(equalTo: tableView.bottomAnchor, constant: -20),
           add.heightAnchor.constraint(equalToConstant: 50),
           add.widthAnchor.constraint(equalToConstant: 80),
       ])
   }
   
   @objc func addToDoneList() {
       var steps = ClipBoard.clipBoard.stepsForGoal // массив  шагов
       let indexes = toDoneArray// индексы которые нужно добавить
       var newArray:[String] = []

       for number in indexes {
           newArray.append(steps[number])
           let indexPath = IndexPath(row: number, section: 2)
           tableView.reloadRows(at: [indexPath], with: .right)
       }
       steps.removeAll(where: {newArray.contains($0)})
       toDoneArray = []
       ClipBoard.clipBoard.stepsForGoal = steps
       ClipBoard.clipBoard.doneArray = ClipBoard.clipBoard.doneArray + newArray
       addState = .hidden
       showAddButtonAndAddToDoneArray()
       tableView.reloadData()
   }

   //MARK: main methods for tableView
    
   func numberOfSections(in tableView: UITableView) -> Int {
       if progressState == .willDoList {
           return 4
       } else {
           return 3
       }
   }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       switch section {
       case 0: return 1
       case 1: return 1
       case 2:
           if  progressState == whatList.willDoList {
               return ClipBoard.clipBoard.stepsForGoal.count
           } else {
               return ClipBoard.clipBoard.doneArray.count + (ClipBoard.clipBoard.stepsForGoal.count - ClipBoard.clipBoard.stepsForGoal.count)
           }
       case 3: return 1
       default:  return 1
       }
   }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
       switch indexPath.section {
           // Image, Name and Describe of goal
       case 0:
           let topCell = tableView.dequeueReusableCell(withIdentifier: TopCell.identifier) as! TopCell
           topCell.editCase = self
           topCell.reloadTable = self
           topCell.callImagePickerDelegate = self
           
           switch editIs {
           case .on:
               topCell.imageOfGoal.image = visualOfGoal
               topCell.changeButton.setTitle("Done", for: .normal)
               topCell.changeButton.backgroundColor = .activeElementBackground
               topCell.nameOfGoal.isUserInteractionEnabled = true
               topCell.nameOfGoal.font = .T1
               topCell.nameOfGoal.textColor = .secondaryText
               topCell.reasonDescribe.isUserInteractionEnabled = true
               topCell.reasonDescribe.textColor = .secondaryText
               topCell.reasonDescribe.font = .T1
               topCell.plusButton.isHidden = false
               topCell.clearButton.isHidden = false
               topCell.clearButton2.isHidden = false
               tableView.isScrollEnabled = false
           case .off:
               topCell.imageOfGoal.image = visualOfGoal
               topCell.changeButton.setTitle("Edit", for: .normal)
               topCell.changeButton.backgroundColor = .disabledElementBackground
               topCell.nameOfGoal.isUserInteractionEnabled = false
               topCell.nameOfGoal.textColor = .primaryText
               topCell.nameOfGoal.font = .H3
               topCell.reasonDescribe.isUserInteractionEnabled = false
               topCell.reasonDescribe.textColor = .primaryText
               topCell.reasonDescribe.font = .T1
               topCell.plusButton.isHidden = true
               topCell.clearButton.isHidden = true
               topCell.clearButton2.isHidden = true
               tableView.isScrollEnabled = true
           }
           return topCell
           
           // Switcher of list WillDo and Done
       case 1:
           let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchCell.identifier) as! SwitchCell
           switchCell.delegate = self
           switchCell.delegateState = self
           switchCell.reloadToDoListDelegate = self
                       
           switch progressState {
           case .willDoList:
               switchCell.willDo.backgroundColor = .disabledElementBackground
               switchCell.willDo.layer.cornerRadius = 20
               switchCell.done.backgroundColor = .primaryBackground
           case .doneList:
               switchCell.willDo.backgroundColor = .primaryBackground
               switchCell.done.layer.cornerRadius = 20
               switchCell.done.backgroundColor = .disabledElementBackground
           }
           return switchCell
           
           // Cells of WillDo of Done
       case 2:                      
           switch progressState {
           case .willDoList:
               let willDoCell = tableView.dequeueReusableCell(withIdentifier: WillDoCell.identifier) as! WillDoCell
               willDoCell.selectionStyle = UITableViewCell.SelectionStyle.none

               if toDoneArray.contains(indexPath.row){
                   willDoCell.checkImageBox.image = UIImage(named:"CheckIcon")
               }
               
               willDoCell.textView.text = ClipBoard.clipBoard.stepsForGoal[indexPath.row]
               return willDoCell
               
           case .doneList:
               let doneCell = tableView.dequeueReusableCell(withIdentifier: DoneCell.identifier) as! DoneCell
               doneCell.selectionStyle = UITableViewCell.SelectionStyle.none
               doneCell.textView.text = ClipBoard.clipBoard.doneArray[indexPath.row]
               return doneCell
           }
           // Cell - Add some steps
       case 3:
           let addCell = tableView.dequeueReusableCell(withIdentifier: AddCell.identifier) as! AddCell
           return addCell

       default:
           let willDoAndDoneCell = tableView.dequeueReusableCell(withIdentifier: WillDoCell.identifier) as! WillDoCell
           return willDoAndDoneCell
       }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
       if progressState == .willDoList {
           switch indexPath.section {
           case 2:
               if toDoneArray.contains(indexPath[1]) {
                   takeAndRemoveIndexOfCell(index: indexPath[1], array: toDoneArray)
                   toDoneArray.sort()
                   showAddButtonAndAddToDoneArray()
                   tableView.reloadData()

               } else {
                   takeAndAddIndexOfCell(index: indexPath[1], array: toDoneArray)
                   toDoneArray.sort()
                   showAddButtonAndAddToDoneArray()
                   tableView.reloadData()
               }
           case 3:
               let newVC = AddBottomSheetController()

               newVC.delegate = self

               if let sheet = newVC.sheetPresentationController {
                   sheet.detents = [ .large(),]
                   sheet.prefersScrollingExpandsWhenScrolledToEdge = false
                   sheet.prefersGrabberVisible = false
                   sheet.preferredCornerRadius = 22
               }
               present(newVC, animated: true, completion: nil)
           default: break
           }
       }
   }
   
   //MARK: - Other methods for tableView --------
   func showAddButtonAndAddToDoneArray() {
       if toDoneArray.isEmpty {
           add.isHidden = true
       } else {
           add.isHidden = false
       }
   }
   
   func takeAndAddIndexOfCell(index:Int, array:[Int] ) {
       var array = self.toDoneArray
       array.append(index)
       toDoneArray = array
   }

   func takeAndRemoveIndexOfCell(index:Int, array:[Int] ) {
       var array = self.toDoneArray
       if let index = array.firstIndex(of: index) {
           array.remove(at: index)
       }
       toDoneArray = array
   }

   func scrollToBottom(){
           DispatchQueue.main.async {
               let indexPath = IndexPath(row: 0, section: 3)
               self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
           }
       }

   func reloadList() {
       switch progressState {
       case .willDoList:
           toDoneArray = []
       case .doneList:
           add.isHidden = true
           toDoneArray = []
       }
   }

    // MARK: -  Save to JSON-file and segue to main list of goals
   @objc func toMainView() {
       var arrayOfGoalData: Data?
       let manager = FileManager.default
       guard let url = manager.urls(for: .documentDirectory,
                                    in: .userDomainMask).first else {return}
       // создаем ссылку для папки
       let folderUrl = url.appendingPathComponent(folderName)

       // создаем ссылку для изображения в папке
       let imageUrl = folderUrl.appendingPathComponent("image_\(ClipBoard.clipBoard.imageName)")

       let urlImage = URL(string: ClipBoard.clipBoard.imageURL)
       let data = try? Data(contentsOf: urlImage!)
       let newImage = UIImage(data: data!)
       let imageData = newImage?.jpegData(compressionQuality: 1.0)

       manager.createFile(atPath: imageUrl.path,
                          contents: imageData)
       ClipBoard.clipBoard.imageURL = "file://" + imageUrl.path

      do {
           // создаем папку по ссылке
           try manager.createDirectory(at: folderUrl,
                                       withIntermediateDirectories: true)
          print(folderUrl)
      } catch {
           print(error)
       }
       
       let goalCard = Goal(imageURL: ClipBoard.clipBoard.imageURL,
                           name: ClipBoard.clipBoard.name,
                           reason: ClipBoard.clipBoard.reason,
                           stepsForGoal: ClipBoard.clipBoard.stepsForGoal,
                           doneArray: ClipBoard.clipBoard.doneArray)
       
       arrayOfGoals.append(goalCard)
       
       do {
           arrayOfGoalData = try JSONEncoder().encode(arrayOfGoals)
       } catch {
           print(error.localizedDescription)
       }

       // создаем  файл в папке по ссылке
       let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
       manager.createFile(atPath: fileUrl.path,
                          contents: arrayOfGoalData)

    //------------------------------------
       //обнуляем буфер
       visualOfGoal = UIImage(named:"")
       ClipBoard.clipBoard.imageURL = ""
       ClipBoard.clipBoard.name = ""
       ClipBoard.clipBoard.reason = ""
       ClipBoard.clipBoard.doneArray.removeAll()
       ClipBoard.clipBoard.stepsForGoal.removeAll()
       arrayOfGoals = []
       //переходим на новый экран
       let newVC = MainViewOfGoalsView()
       navigationController?.pushViewController(newVC, animated: true)
   }

   //MARK: - State of list
   //MARK: - Enums
   enum stateAddButton {
       case show
       case hidden
   }
   //which list will show
   enum whatList {
       case willDoList
       case doneList
   }
   //editable mode for first(topСell) On or Off
   enum editStateOfTopCell {
       case on
       case off
   }
   
   //MARK: Methods/Switchers for enums
   func callAlert() {
       let text = "Fill all fields"
       
       let alertControl = UIAlertController(title: nil, message: text, preferredStyle: .alert)
       present(alertControl, animated: true, completion: nil)
       DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
           alertControl.dismiss(animated: true, completion: nil)
       }
   }

   func editDoneButton(){
       switch editIs {
       case .on:
           tableView.reloadData()
           viewDidLoad()
           navigationController?.isNavigationBarHidden = false
           navigationController?.hidesBarsOnSwipe = true
           editIs = .off
       case .off:
           tableView.reloadData()
           viewDidLoad()
           navigationController?.isNavigationBarHidden = true
           navigationController?.hidesBarsOnSwipe = false
           editIs = .on
       }
   }
   
   func emptyCheck(text: String) -> Bool {
       var value = false
       if text.isEmpty {
           value = true
       }
       return value
   }
   
   func reload() {
       tableView.reloadData()
   }
   
   func changeToWillDo() {
       progressState = .willDoList
   }
   
   func changeToDone() {
       progressState = .doneList
   }
   
   //MARK: ImagePickerController
   func callImagePicker() {
       setImage()
   }
   
   func setImage() {
       let vc = UIImagePickerController()
       vc.sourceType = .photoLibrary
       vc.delegate = self
       vc.allowsEditing = true
       present(vc, animated: true)
   }
   
    var ifImageWillChangeAgain = 1
    
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
           
           if ifImageWillChangeAgain == 1 {
               deleteImage(url: ClipBoard.clipBoard.imageURL)
               ifImageWillChangeAgain = 0
           }

           if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
               ClipBoard.clipBoard.imageURL = imgUrl.absoluteString
               ClipBoard.clipBoard.imageName = imgUrl.lastPathComponent
               savePhoto(img: image)
               ifImageWillChangeAgain = 1
           }
           visualOfGoal = image
           
           let indexPath = IndexPath.init(row: 0, section: 0)
           tableView.reloadRows(at: [indexPath], with: .bottom)
       }
       picker.dismiss(animated: true, completion: nil)
   }
    
    func deleteImage(url:String){
        let manager = FileManager.default
        guard let urlImage = URL(string: url) else { return }
        do {
            try manager.removeItem(at: urlImage)
        } catch {
            print(error)
        }
    }
    
    func savePhoto(img: UIImage){
        let manager = FileManager.default
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        let folderUrl = url.appendingPathComponent(folderName)
        
        let imageUrl = folderUrl.appendingPathComponent("image_\(ClipBoard.clipBoard.imageName)")

        let data: NSData = img.jpegData(compressionQuality: 1)! as NSData
        let newImage = UIImage(data: data as Data)
        let imageData = newImage?.jpegData(compressionQuality: 1.0)
        
        manager.createFile(atPath: imageUrl.path,
                           contents: imageData)
        ClipBoard.clipBoard.imageURL = "file://" + imageUrl.path

       do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
           print(folderUrl)
       } catch {
            print(error)
        }
    }

   func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       picker.dismiss(animated: true, completion: nil)
   }
}
