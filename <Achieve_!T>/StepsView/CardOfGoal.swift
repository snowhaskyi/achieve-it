//
//  CardOfGoal.swift
//  Achieve_IT

import UIKit

final class CardOfGoal: UIViewController,
                        UITableViewDelegate,
                        UITableViewDataSource {
        
    weak var delegate: DeleteGoal?
    
    var willDoArray:[String] = []
    var doneArray:[String] = []
    var index: Int? = nil
    
    let topLine = UILabel()
    var name = UILabel()
    let describe = UITextView()
    let deleteButton = UIButton()
    let editButton = UIButton()
    let image = UIImageView()
    let frameLabel = UILabel() // just little dash-line for decorating
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .primaryBackground
        
        let itemArrow = [topLine, image, frameLabel, name, describe, deleteButton, editButton, tableView, ]
        for i in itemArrow {
            i.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(i)
        }
        setup()
        tableViewSetup()
        constraint()
    }
    
    func tableViewSetup() {
    
        tableView.backgroundColor = .primaryBackground
        tableView.layer.cornerRadius = 15
        tableView.separatorColor = .clear
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(WillDoCell.self, forCellReuseIdentifier: WillDoCell.identifier)
        tableView.register(DoneCell.self, forCellReuseIdentifier: DoneCell.identifier)
    }
    
    func setup() {
        topLine.layer.borderWidth = 3
        topLine.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)

        image.layer.cornerRadius = 15
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        frameLabel.frame = CGRect(x: 50, y: 20, width: 120, height: 150)
        frameLabel.addDashedBorder()
                
        name.backgroundColor = .primaryBackground
        name.textColor = .primaryText
        name.textAlignment = .center
        name.font = .H3

        describe.backgroundColor = .primaryBackground
        describe.textColor = .primaryText
        describe.font = .T1
        describe.isEditable = false
        
        deleteButton.backgroundColor = .red
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.setTitleColor(.white, for: .normal)
        deleteButton.titleLabel?.font = .A1
        deleteButton.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        deleteButton.layer.cornerRadius = 10
        deleteButton.addTarget(self, action: #selector(callCheckAlert), for: .touchUpInside)
        //deleteButton.addTarget(self, action: #selector(showDidItAlert), for: .touchUpInside)
        
        editButton.isHidden = true
        editButton.backgroundColor = .activeElementBackground
        editButton.setTitle("Done", for: .normal)
        editButton.setTitleColor(.white, for: .normal)
        editButton.titleLabel?.font = .A1
        editButton.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        editButton.layer.cornerRadius = 10
        editButton.addTarget(self, action: #selector(addToDoneList), for: .touchUpInside)
    }
    
    func constraint() {
        NSLayoutConstraint.activate([
            topLine.heightAnchor.constraint(equalToConstant: 5),
            topLine.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            topLine.widthAnchor.constraint(equalToConstant: 100),
            topLine.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            image.widthAnchor.constraint(equalToConstant: 120),
            image.heightAnchor.constraint(equalToConstant: 150),
            
            frameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            frameLabel.centerXAnchor.constraint(equalTo: image.centerXAnchor),
            frameLabel.widthAnchor.constraint(equalToConstant: 120),
            frameLabel.heightAnchor.constraint(equalToConstant: 150),
            
            name.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            name.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 10),
            name.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            name.heightAnchor.constraint(equalToConstant: 50),
            
            describe.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10),
            describe.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 10),
            describe.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            describe.bottomAnchor.constraint(equalTo: image.bottomAnchor, constant: 0),
            
            tableView.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 20),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            deleteButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 10),
            deleteButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            deleteButton.heightAnchor.constraint(equalToConstant: 50),
            deleteButton.widthAnchor.constraint(equalToConstant: 70),
            
            editButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 10),
            editButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            editButton.heightAnchor.constraint(equalToConstant: 50),
            editButton.widthAnchor.constraint(equalToConstant: 70),
        ])
    }
    
//MARK: TableView methods
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Will Do"
        case 1:
            return "Done"
        default:
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return willDoArray.count
        case 1:
            return doneArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let doneCell = tableView.dequeueReusableCell(withIdentifier: DoneCell.identifier) as! DoneCell
        doneCell.selectionStyle = UITableViewCell.SelectionStyle.none

        let willDoCell = tableView.dequeueReusableCell(withIdentifier: WillDoCell.identifier) as! WillDoCell
        willDoCell.selectionStyle = UITableViewCell.SelectionStyle.none

        switch indexPath.section {
        //willDoArray
        case 0:
            if indexesOfCheckCell.contains(indexPath.row) {
                willDoCell.checkImageBox.image = UIImage(named:"CheckIcon")
            }
        
            willDoCell.textView.text = willDoArray[indexPath.row]
            return willDoCell
            
        //doneArray
        case 1:
            doneCell.textView.text = doneArray[indexPath.row]
            return doneCell
        default:
            doneCell.textView.text = "Oops"
             return willDoCell
        }
    }
   //MARK: Methods
        
    var indexesOfCheckCell:[Int] = []
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if !indexesOfCheckCell.contains(indexPath[1]) {
                indexesOfCheckCell.append(indexPath[1])
                print(indexesOfCheckCell)
            } else {
                takeAndRemoveIndexOfCell(index: indexPath.row, array: indexesOfCheckCell)
                print(indexesOfCheckCell)
            }
            showOrHideDoneButton()
            indexesOfCheckCell.sort()
            tableView.reloadData()
            print(indexesOfCheckCell)
        }
    }
    
    @objc func addToDoneList() {
        var steps = willDoArray// массив  шагов
        let indexes = indexesOfCheckCell// индексы которые нужно добавить
        var newArray:[String] = []

        for number in indexes {
            newArray.append(steps[number])
            let indexPath = IndexPath(row: number, section: 0)
            tableView.reloadRows(at: [indexPath], with: .right)
        }
        steps.removeAll(where: {newArray.contains($0)})
        indexesOfCheckCell = []
        willDoArray = steps
        doneArray = doneArray + newArray
        showOrHideDoneButton()
        
        if willDoArray.isEmpty {
            showDidItAlert()
        }
        
        delegate?.doChangesInGoal(index: index!, willArray: willDoArray, doneArray: doneArray)
        tableView.reloadData()
    }
    
    func takeAndAddIndexOfCell(index:Int, array:[Int] ) {
        var array = indexesOfCheckCell
        array.append(index)
        indexesOfCheckCell = array
    }

    func takeAndRemoveIndexOfCell(index:Int, array:[Int] ) {
        var array = self.indexesOfCheckCell
        if let index = array.firstIndex(of: index) {
            array.remove(at: index)
        }
        indexesOfCheckCell = array
    }
    
    func showOrHideDoneButton() {
        if indexesOfCheckCell.isEmpty {
            editButton.isHidden = true
        } else {
            editButton.isHidden = false
        }
    }
    
//MARK: Alerts
    func showDidItAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "YES!! I did IT!!", style: .default, handler: nil)
        
        let imageGif = UIImage.gifImageWithName("DidIt")
        let imgViewTitle = UIImageView(frame:CGRect(x: 0,
                                                    y: -180,
                                                    width: 270,
                                                    height: 180))
        imgViewTitle.image = imageGif
        imgViewTitle.layer.cornerRadius = 30
        imgViewTitle.clipsToBounds = true
        imgViewTitle.contentMode = .scaleAspectFill
        imgViewTitle.backgroundColor = .red
        
        alert.view.addSubview(imgViewTitle)
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func callCheckAlert() {
        let alertControl = UIAlertController(title: "Are you sure?", message: "You want delete this dream", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        let okAction = UIAlertAction(title: "Yes", style: .default) { [self] _ in
            deleteGoal()
        }
        alertControl.addAction(okAction)
        alertControl.addAction(cancelAction)
        present(alertControl,animated: true)
    }
//MARK: Other methods
    @objc func deleteGoal() {
        delegate?.deleteGoal()
        delegate?.reloadCollectionView()
        self.dismiss(animated: true)
    }
}
