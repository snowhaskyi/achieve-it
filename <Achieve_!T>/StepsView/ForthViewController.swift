//
//  ForthViewController.swift
//  Achieve_IT
import UIKit

final class ForthViewController: UIViewController,
                                 UITableViewDelegate,
                                 UITableViewDataSource,
                                 UIPopoverPresentationControllerDelegate,
                                 UITextViewDelegate,
                                 ReloadTableViewDelegate,
                                 EditSheetBottomController {
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(Main.self, forCellReuseIdentifier: Main.identifierCell)
        tableView.register(AddButtonCell.self, forCellReuseIdentifier: AddButtonCell.identifierCell)
        return tableView
    }()
    
    let label = UILabel()
    let textView = UITextView()
    let continueButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.hidesBarsOnSwipe = false
        view.backgroundColor = .primaryBackground
        
        let items = [label, textView, tableView, continueButton]
        for i in items {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        tableViewSet()
        backBarButton()
        constraint()
        setup()
    }
            
    func tableViewSet() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.separatorColor = .activeElementBackground
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .init(top: 0, left: 0, bottom: 0, right: 300)
    }
        
    func scrollToBottom(){
        let indexPath = IndexPath(row: ClipBoard.clipBoard.stepsForGoal.count, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        scrollToBottom()
    }
    
    @objc func continueButtonAction() {
        let newVC = GoalCard()
        navigationController?.pushViewController(newVC, animated: true)
    }

    func setup() {
        label.text = "May be some steps?"
        label.textColor = .primaryText
        label.font = UIFont(name: "SFProDisplay-Regular", size: 38)
        
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.text = "What steps should be taken now to achieve this dream? You can skip it, if not sure"
        textView.backgroundColor = .primaryBackground
        textView.textColor = .secondaryText
        textView.lineMode()
        textView.font = .H6
                
        continueButton.backgroundColor = .activeElementBackground
        continueButton.layer.cornerRadius = 22
        continueButton.setTitle("Continue", for: .normal)
        continueButton.addTarget(self, action: #selector(continueButtonAction), for: .touchUpInside)
        continueButton.setTitleColor(.activeElementText, for: .normal)
        continueButton.titleLabel?.font = .A1
        }
    
    func constraint() {
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 110),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
            label.heightAnchor.constraint(equalToConstant: 48),
            label.widthAnchor.constraint(equalToConstant: 370),
            
            textView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            textView.heightAnchor.constraint(equalToConstant: 100),

            tableView.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
            tableView.bottomAnchor.constraint(equalTo: continueButton.topAnchor, constant: -10),
            
            continueButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            continueButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            continueButton.widthAnchor.constraint(equalToConstant:  327),
            continueButton.heightAnchor.constraint(equalToConstant: 48),
        ])
    }
}

//MARK: TABLEVIEW DATASOURSE
extension ForthViewController {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 && ClipBoard.clipBoard.stepsForGoal.count == 0 {
            return false
        } else if indexPath.row == ClipBoard.clipBoard.stepsForGoal.count{
            return false
        } else {
            return true
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            ClipBoard.clipBoard.stepsForGoal.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    func reload(){
        tableView.reloadData()
        viewDidLoad()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ClipBoard.clipBoard.stepsForGoal.count == 0 {
            return 1
        } else {
            return ClipBoard.clipBoard.stepsForGoal.count + 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stepsCount = ClipBoard.clipBoard.stepsForGoal.count
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Main.identifierCell) as! Main
        cell.delegate = self
        cell.callEditSheetBottom = self

        if ClipBoard.clipBoard.stepsForGoal.isEmpty{
            let addCell = tableView.dequeueReusableCell(withIdentifier: AddButtonCell.identifierCell) as! AddButtonCell
            return addCell
        } else if ClipBoard.clipBoard.stepsForGoal.count != 0 {
            switch indexPath.row {
            case stepsCount:
                let addCell = tableView.dequeueReusableCell(withIdentifier: AddButtonCell.identifierCell) as! AddButtonCell
                return addCell
            default:
                cell.textView.text = ClipBoard.clipBoard.stepsForGoal[indexPath.row]
                cell.index = indexPath.row
                return cell
            }
        } else {
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addCell = ClipBoard.clipBoard.stepsForGoal.count
        
        if indexPath.row == addCell {
            popOver()
        }
    }

    func popOver() {
        let newVC = AddBottomSheetController()
        newVC.delegate = self
        
        if let sheet = newVC.sheetPresentationController {
            sheet.detents = [.medium()]
            sheet.prefersScrollingExpandsWhenScrolledToEdge = false
            sheet.prefersGrabberVisible = false
            sheet.preferredCornerRadius = 22
        }
        present(newVC, animated: true, completion: nil)
    }
    
    func check() {
        print("it is work")
    }
    
    func callEditBottomSheet(x:Int) {
        let newVC = EditBottomSheetController()

        newVC.delegate = self
        newVC.editDelegate = self
        newVC.indexOfCell = x
        newVC.content = ClipBoard.clipBoard.stepsForGoal[x]
    
        if let sheet = newVC.sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.prefersScrollingExpandsWhenScrolledToEdge = false
            sheet.prefersGrabberVisible = false
            sheet.preferredCornerRadius = 22
        }
        present(newVC, animated: true, completion: nil)
    }
}
