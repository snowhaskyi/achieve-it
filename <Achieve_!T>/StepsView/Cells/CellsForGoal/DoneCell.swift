//
//  DoneCell.swift
//  Achieve_IT

import UIKit

class DoneCell: UITableViewCell {
    
    static  let identifier = "DoneCell"

    let textView = PaddingLabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let items = [textView]
        
        for item in items {
            contentView.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }

        self.backgroundColor = .primaryBackground
        contentView.layer.cornerRadius = 12
        contentView.backgroundColor = .secondaryBackGround
        constraint()

        setup()
    }

    override func layoutSubviews() {
       super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by:  UIEdgeInsets(top: 0, left: 16, bottom: 10, right: 16))
        }

    func constraint() {
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),
            textView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
            textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 44),
        ])
    }

    func setup() {
        textView.textColor = .primaryText
        textView.lineMode()
        textView.font = .T1
        textView.padding( 5, 5, 5, 0)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
