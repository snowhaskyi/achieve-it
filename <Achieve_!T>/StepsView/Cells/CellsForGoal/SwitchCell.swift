//
//  SwitchCell.swift
//  Achieve_IT

import UIKit

final class SwitchCell: UITableViewCell {
        
    static let identifier = "identifierCell"

    weak var delegate: ReloadTableViewDelegate?
    weak var delegateState: ChangeStateOFinalList?
    weak var reloadToDoListDelegate: ReloadToDoListDelegate?

    let willDo = UIButton()
    let done = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.backgroundColor = .primaryBackground
        
        contentView.addSubview(willDo)
        willDo.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(done)
        done.translatesAutoresizingMaskIntoConstraints = false

        constraint()
        setup()
    }

    func constraint(){
        NSLayoutConstraint.activate([
            willDo.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            willDo.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            willDo.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            willDo.widthAnchor.constraint(equalToConstant: 179),
            willDo.heightAnchor.constraint(equalToConstant: 39),
            
            done.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            done.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            done.leadingAnchor.constraint(equalTo: willDo.trailingAnchor, constant: 0),
            done.widthAnchor.constraint(equalToConstant: 179),
            done.heightAnchor.constraint(equalToConstant: 39)
        ])
    }

    func setup(){
                
        willDo.backgroundColor = .primaryBackground
        willDo.setTitle("Will do", for: .normal)
        willDo.setTitleColor(.secondaryText, for: .normal)
        willDo.titleLabel?.font = .A1
        willDo.addTarget(self, action: #selector(changeToWillDo), for: .touchUpInside)

        done.backgroundColor = .primaryBackground
        done.setTitle("Done", for: .normal)
        done.setTitleColor(.secondaryText, for: .normal)
        done.titleLabel?.font = .A1
        done.addTarget(self, action: #selector(changeToDone), for: .touchUpInside)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - State of list
    @objc func changeToDone() {
        delegateState?.changeToDone()
        delegate?.reload()
        reloadToDoListDelegate?.reloadList()
    }

    @objc func changeToWillDo() {
        delegateState?.changeToWillDo()
       delegate?.reload()
        reloadToDoListDelegate?.reloadList()
    }
}
