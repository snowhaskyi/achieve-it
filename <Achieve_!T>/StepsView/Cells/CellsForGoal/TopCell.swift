//
//  TopCell.swift
//  Achieve_IT


import UIKit

class TopCell: UITableViewCell {
    
    static let identifier = "identifier"
    
    weak var editCase: СhangeEditState?
    weak var reloadTable: ReloadTableViewDelegate?
    weak var callImagePickerDelegate: CallImagePickerDelegate?
    
    let imageOfGoal = UIImageView()
    let backgroundForImageOfGoal = UIView()
    let nameOfGoal = UITextField()
    let reasonDescribe = UITextView()
    let stepsToAchieve = UILabel()
    let changeButton = UIButton()
    let plusButton = UIButton()
    let clearButton = UIButton()
    let clearButton2 = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.backgroundColor = .primaryBackground
        
        let items = [backgroundForImageOfGoal, imageOfGoal, nameOfGoal, reasonDescribe, stepsToAchieve, changeButton, plusButton, clearButton, clearButton2]
        
        for item in items {
            contentView.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }

        constraint()
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(){
        let image = UIImage(named: "PlusNew")
        backgroundForImageOfGoal.backgroundColor = .primaryBackground
        
        imageOfGoal.contentMode = .scaleAspectFill
        imageOfGoal.clipsToBounds = true
        imageOfGoal.layer.cornerRadius = 12

        plusButton.backgroundColor = .activeElementBackground.withAlphaComponent(0.2)
        plusButton.setImage(image, for: .normal)
        plusButton.layer.cornerRadius = 30
        plusButton.addTarget(self, action: #selector(callImagePicker), for: .touchUpInside)
        plusButton.setTitleColor(.activeElementText, for: .normal)
        plusButton.titleLabel?.font = .A1
        plusButton.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        clearButton.setImage(UIImage(named: "ClearArrow"), for: .normal)
        clearButton.contentMode = .scaleAspectFit
        clearButton.layer.cornerRadius = 19
        clearButton.addTarget(self, action: #selector(clearText), for: .touchUpInside)
        clearButton.setTitleColor(.activeElementText, for: .normal)
        clearButton.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        clearButton2.setImage(UIImage(named: "ClearArrow"), for: .normal)
        clearButton2.contentMode = .scaleAspectFit
        clearButton2.layer.cornerRadius = 19
        clearButton2.addTarget(self, action: #selector(clearText2), for: .touchUpInside)
        clearButton2.setTitleColor(.activeElementText, for: .normal)
        clearButton2.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        nameOfGoal.text = ClipBoard.clipBoard.name
        nameOfGoal.font = .H3
        nameOfGoal.textColor = .primaryText
        
        reasonDescribe.text = ClipBoard.clipBoard.reason
        reasonDescribe.textColor = .primaryText
        reasonDescribe.font = .T1
        reasonDescribe.backgroundColor = .primaryBackground

        stepsToAchieve.text = "Steps to achieve it"
        stepsToAchieve.textAlignment = .center
        stepsToAchieve.textColor = .primaryText
        stepsToAchieve.font = .H6
        stepsToAchieve.lineMode()

        changeButton.backgroundColor = .activeElementBackground
        changeButton.layer.cornerRadius = 22
        changeButton.addTarget(self, action: #selector(switchEditDone), for: .touchUpInside)
        changeButton.setTitleColor(.activeElementText, for: .normal)
        changeButton.titleLabel?.font = .A1
        changeButton.contentEdgeInsets =  UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
    
    @objc func clearText() {
        nameOfGoal.text = ""
        nameOfGoal.placeholder = "goal's name"
        nameOfGoal.becomeFirstResponder()
    }
    
    @objc func clearText2() {
        reasonDescribe.text = ""
        //reasonDescribe.placeholder = "reason of your goal"
        reasonDescribe.becomeFirstResponder()
    }

    @objc func switchEditDone() {
        if nameOfGoal.text == "" || reasonDescribe.text == "" {
            editCase?.callAlert()
        } else {
            editCase?.editDoneButton()
            reloadTable?.reload()
        }
    }
    
    @objc func callImagePicker() {
        callImagePickerDelegate?.callImagePicker()
    }
    
    func constraint(){
        NSLayoutConstraint.activate([
            clearButton.topAnchor.constraint(equalTo: nameOfGoal.topAnchor, constant: 2),
            clearButton.bottomAnchor.constraint(equalTo: nameOfGoal.topAnchor, constant: -2),
            clearButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            clearButton.heightAnchor.constraint(equalToConstant: 38),
            clearButton.widthAnchor.constraint(equalToConstant: 38),
            
            clearButton2.topAnchor.constraint(equalTo: reasonDescribe.topAnchor, constant: 2),
            clearButton2.bottomAnchor.constraint(equalTo: reasonDescribe.topAnchor, constant: -2),
            clearButton2.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            clearButton2.heightAnchor.constraint(equalToConstant: 38),
            clearButton2.widthAnchor.constraint(equalToConstant: 38),

            plusButton.centerXAnchor.constraint(equalTo: imageOfGoal.centerXAnchor),
            plusButton.centerYAnchor.constraint(equalTo: imageOfGoal.centerYAnchor),
            
            backgroundForImageOfGoal.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            backgroundForImageOfGoal.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            backgroundForImageOfGoal.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            backgroundForImageOfGoal.heightAnchor.constraint(equalToConstant: 298),

            imageOfGoal.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            imageOfGoal.centerXAnchor.constraint(equalTo: backgroundForImageOfGoal.centerXAnchor),
            imageOfGoal.centerYAnchor.constraint(equalTo: backgroundForImageOfGoal.centerYAnchor),
            imageOfGoal.widthAnchor.constraint(equalToConstant: 251),

            nameOfGoal.topAnchor.constraint(equalTo: imageOfGoal.bottomAnchor, constant: 20),
            nameOfGoal.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            nameOfGoal.heightAnchor.constraint(equalToConstant: 38),
            nameOfGoal.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -38),

            reasonDescribe.topAnchor.constraint(equalTo: nameOfGoal.bottomAnchor, constant: 10),
            reasonDescribe.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            reasonDescribe.heightAnchor.constraint(equalToConstant: 70),
            reasonDescribe.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -38),

            stepsToAchieve.topAnchor.constraint(equalTo: reasonDescribe.bottomAnchor, constant: 10),
            stepsToAchieve.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            stepsToAchieve.heightAnchor.constraint(equalToConstant: 32),
            stepsToAchieve.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -38),
            stepsToAchieve.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),

            changeButton.trailingAnchor.constraint(equalTo: imageOfGoal.trailingAnchor, constant: -10),
            changeButton.bottomAnchor.constraint(equalTo: imageOfGoal.bottomAnchor, constant: -10)
        ])
    }
}
