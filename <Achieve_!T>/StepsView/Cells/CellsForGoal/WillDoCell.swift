//
//  WillDoCell.swift
//  Achieve_IT

import UIKit

final class WillDoCell: UITableViewCell {

    static  let identifier = "WillDoCell"

    var textView = PaddingLabel()
    let containerForCheckBox = UIView()
    let checkImageBox = UIImageView()

    override func prepareForReuse() {
        super.prepareForReuse()
        checkImageBox.image = UIImage(named: "")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let items = [textView, containerForCheckBox, checkImageBox]
        
        for item in items {
            contentView.addSubview(item)
            item.translatesAutoresizingMaskIntoConstraints = false
        }

        self.backgroundColor = .primaryBackground
        contentView.layer.cornerRadius = 12
        contentView.backgroundColor = .secondaryBackGround
        constraint()

        setup()
    }

    override func layoutSubviews() {
       super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by:  UIEdgeInsets(top: 0, left: 16, bottom: 10, right: 16))
        }

    func constraint() {
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),
            textView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
            textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 44),
            
            containerForCheckBox.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            containerForCheckBox.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            containerForCheckBox.heightAnchor.constraint(equalToConstant: 26),
            containerForCheckBox.widthAnchor.constraint(equalToConstant: 26),

            checkImageBox.leadingAnchor.constraint(equalTo: containerForCheckBox.leadingAnchor, constant: 4),
            checkImageBox.centerYAnchor.constraint(equalTo: containerForCheckBox.centerYAnchor),
            checkImageBox.heightAnchor.constraint(equalToConstant: 18),
            checkImageBox.widthAnchor.constraint(equalToConstant: 18),
        ])
    }

    func setup() {
        checkImageBox.backgroundColor = .primaryBackground
        
        containerForCheckBox.backgroundColor = .primaryBackground
        containerForCheckBox.layer.cornerRadius = 5

        textView.textColor = .primaryText
        textView.lineMode()
        textView.font = .T1
        textView.padding( 5, 5, 5, 0)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
