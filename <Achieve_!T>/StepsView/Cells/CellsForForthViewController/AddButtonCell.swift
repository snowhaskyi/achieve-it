//
//  AddButtonCell.swift
//  Achieve_IT


import UIKit

class AddButtonCell: UITableViewCell {
    
    static let identifierCell =  "identifierButton"
    
    var leadingPad:CGFloat = 10
    let image = UIImageView()
    let label = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.backgroundColor = .primaryBackground
        let items = [image, label]
        for i in items {
            contentView.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        constraints()
        customElement()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   func constraints(){
       NSLayoutConstraint.activate([
        image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: leadingPad),
        image.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        image.heightAnchor.constraint(equalToConstant: 15),
        image.widthAnchor.constraint(equalToConstant: 15),
        
        label.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 20),
        label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        label.heightAnchor.constraint(equalToConstant: 50),
       ])
    }
    
    func customElement(){
        image.image = UIImage(named: "Plus")
        
        label.text = "Add a step"
        label.textColor = .primaryText
        label.font = .H6
    }
}
