//
//  BottomSheetController.swift
//  Achieve_IT

import UIKit

final class AddBottomSheetController: UIViewController, UITextViewDelegate {
    
    let placeholder = "How would you describe this step"
     
    weak var delegate: ReloadTableViewDelegate?
                 
    let label = UILabel() // just little dash-line for decorating
    let textView = UITextView()
    let button = CustomContinueButton()
    
    var someLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        
        view.backgroundColor = .primaryBackground
        let itemArrow = [label, textView, button]
        for i in itemArrow {
            i.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(i)
        }
        setup()
        constraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        textView.becomeFirstResponder()
    }
        
     func setup() {
         label.layer.borderWidth = 3
         label.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
         
         textView.text = placeholder
         textView.textColor = .secondaryText
         textView.backgroundColor = .primaryBackground
         textView.font = .H6
         let newPosition = textView.beginningOfDocument
         textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
                  
         button.setBackgroundColor(.activeElementBackground, for: .normal)
         button.setBackgroundColor(.disabledElementBackground, for: .disabled)
         button.isEnabled = false
         button.layer.cornerRadius = 22
         button.setTitle("Add", for: .normal)
         button.addTarget(self, action: #selector(addStep), for: .touchUpInside)
         button.setTitleColor(.activeElementText, for: .normal)
         button.titleLabel?.font = .A1
     }
    
    func constraint() {
        
        NSLayoutConstraint.activate([
            label.heightAnchor.constraint(equalToConstant: 5),
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            label.widthAnchor.constraint(equalToConstant: 100),
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            textView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 36),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            textView.heightAnchor.constraint(equalToConstant: 200),
                        
            button.heightAnchor.constraint(equalToConstant: 48),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 304),
            view.keyboardLayoutGuide.topAnchor.constraint(equalToSystemSpacingBelow: button.bottomAnchor, multiplier: 4.0),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
    }
}

extension AddBottomSheetController {
    
    @objc func addStep() {
        let x = textView.text
        ClipBoard.clipBoard.stepsForGoal.append(x!)
        textView.text = ""
        button.isEnabled = false
        delegate?.reload()
        dismiss(animated: true)
    }
   
    //MARK: - textView's placeholder

    func textViewDidChange(_ textView: UITextView) {
       button.isEnabled = !textView.text.isEmpty
        if textView.text.isEmpty {
            textView.text = placeholder
            let newPosition = textView.beginningOfDocument
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
        }
   }
   
    func textViewDidEndEditing(_ textView: UITextView) {
       if textView.text == "" {
           textView.text = placeholder
       }
   }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text == placeholder {
            textView.text = ""
        }
        return true
   }
}
