//
//  Main.swift
//  Achieve_IT

import UIKit
class Main: UITableViewCell, UITextViewDelegate {
    
    static let identifierCell = "identifierCell"
    
    weak var delegate: ReloadTableViewDelegate?
    weak var callEditSheetBottom: EditSheetBottomController?
    
    var index: Int? = nil
    let textView = UITextView()
    let image = UIImageView()
    let editButton = UIButton()
    
    var height: Int?
            
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .primaryBackground
        
        let showOnVC = [ textView, image, editButton]
        
        for i in showOnVC {
            contentView.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        constraints()
        customElement()
    }
    
    func customElement() {
        textView.backgroundColor = .primaryBackground
        textView.textColor = .primaryText
        textView.textAlignment = .left
        textView.font = .H6
        textView.isScrollEnabled = false
        textView.isEditable = false
        
        editButton.setImage(UIImage(named: "EditButton"), for: .normal)
        editButton.addTarget(self, action: #selector(callEditShitBottom), for: .touchUpInside)
        
        image.image = UIImage(named: "DeleteSwipe")
    }
        
    func constraints(){
        
            NSLayoutConstraint.activate([
                textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
                textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
                textView.trailingAnchor.constraint(equalTo: image.leadingAnchor, constant: -5),
                textView.leadingAnchor.constraint(equalTo: editButton.trailingAnchor, constant: 5),
                
                image.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant:  -8),
                image.heightAnchor.constraint(equalToConstant: 15),
                image.widthAnchor.constraint(equalToConstant: 15),
                
                editButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
                editButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5),
                editButton.heightAnchor.constraint(equalToConstant: 17),
                editButton.widthAnchor.constraint(equalToConstant: 17),
            ])
        }
    
    @objc func callEditShitBottom() {
        callEditSheetBottom?.callEditBottomSheet(x: index!)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
