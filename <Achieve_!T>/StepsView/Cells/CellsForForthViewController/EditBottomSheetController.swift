//
//  EditBottomSheet.swift
//  <Achieve_!T>
//
//  Created by Wi-Fai on 12.01.2023.
//

import UIKit

class EditBottomSheetController: UIViewController {
    
    weak var delegate: ReloadTableViewDelegate?
    weak var editDelegate: EditSheetBottomController?
    
    var indexOfCell: Int? = nil
    var content: String? = nil

    let line = UILabel()
    let textView = UITextView()
    let doneButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .primaryBackground.withAlphaComponent(1)
        
        let itemsOnView = [line, textView, doneButton]
        for i in itemsOnView {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        constraint()
        setup()
    }
    
    func setup() {
        line.layer.borderWidth = 3
        line.layer.borderColor = .init(red: 100, green: 100, blue: 0, alpha: 1)
        line.backgroundColor = .yellow
        
        textView.text = content
        textView.textColor = .secondaryText
        textView.backgroundColor = .primaryBackground
        textView.font = .H6
        let newPosition = textView.beginningOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
        
        doneButton.backgroundColor = .activeElementBackground
        doneButton.layer.cornerRadius = 22
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(finishEdit), for: .touchUpInside)
        doneButton.setTitleColor(.activeElementText, for: .normal)
        doneButton.titleLabel?.font = .A1
    }

    func constraint() {
        NSLayoutConstraint.activate([
            line.heightAnchor.constraint(equalToConstant: 5),
            line.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            line.widthAnchor.constraint(equalToConstant: 100),
            line.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            textView.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 36),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            textView.heightAnchor.constraint(equalToConstant: 200),
            
            doneButton.heightAnchor.constraint(equalToConstant: 48),
            doneButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 304),
            view.keyboardLayoutGuide.topAnchor.constraint(equalToSystemSpacingBelow: doneButton.bottomAnchor, multiplier: 4.0),
            doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        textView.becomeFirstResponder()
    }
    
// MARK: Methods
    @objc func finishEdit() {
        if textView.text.isEmpty {
            ClipBoard.clipBoard.stepsForGoal.remove(at: indexOfCell!)
        } else {
            ClipBoard.clipBoard.stepsForGoal[indexOfCell!] = textView.text
        }
        
        delegate?.reload()
        dismiss(animated: true)
    }
}
