//
//  AddCellForMainListOfGoal.swift
//  Achieve_IT

import UIKit

class AddCellForMainListOfGoal: UICollectionViewCell {

    static let identifier = "AddCellForMainListOfGoal"

    private let image = UIImageView()

    override init(frame:CGRect){
        super.init(frame: frame)

        self.contentView.backgroundColor = .secondaryBackGround
        contentView.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        self.clipsToBounds = true

        setup()
        constraint()
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    func setup() {
        image.image = UIImage(named: "Plus")
    }

    func constraint(){
        NSLayoutConstraint.activate([
            image.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            image.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            image.heightAnchor.constraint(equalToConstant: 40),
            image.widthAnchor.constraint(equalToConstant: 40),
        ])
    }
}
