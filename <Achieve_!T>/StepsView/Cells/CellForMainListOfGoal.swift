//
//  CellForMainListOfGoal.swift
//  Achieve_IT

import UIKit

class CellForMainListOfGoal: UICollectionViewCell {
    
    static let identifier = "CellForMainListOfGoal"
    
    let image = UIImageView()
    let label = UILabel()
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.contentView.backgroundColor = .secondaryBackGround
        
        contentView.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        setup()
        constraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    func setup() {
        image.backgroundColor = .black
        image.contentMode = .scaleAspectFill
        
        label.backgroundColor = .secondaryBackGround
        label.text = "Goal!!"
        label.textColor = .primaryText
        label.font = .T2
        label.textAlignment = .center
    }
    
    func constraint(){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            image.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            image.bottomAnchor.constraint(equalTo: label.topAnchor, constant: 0),
            
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            label.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
}
