//
//  ThirdImagePickViewController.swift
//  Achieve_IT
//

import UIKit

final class ThirdImagePickViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    let plusBackground = UIImageView()
    let labelFirst = UILabel()
    let label = UILabel()
    let frameLabel = UILabel() // Рамка для пустого слота
    let imageItem = UIImageView() // Емкость для изображения "цели"
    let imagePlus = UIImageView() // Отображение плюса по центру imageItem
    let button = CustomContinueButton()
    let fieldForImageItem = UIView()
    
    var plusVision:state = .visible
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.hidesBarsOnSwipe = false
        view.backgroundColor = .primaryBackground
        
        let items = [fieldForImageItem, labelFirst, label, frameLabel,  imageItem, button]
                
        for i in items {
            view.addSubview(i)
            i.translatesAutoresizingMaskIntoConstraints = false
        }
        
        if plusVision == .visible {
            visible()
        }

        setup()
        constraints()
        backBarButton()
    }
    //Переделать в кнопку с настройками isHidden
    func visible() {
        view.addSubview(plusBackground)
        plusBackground.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imagePlus)
        imagePlus.translatesAutoresizingMaskIntoConstraints = false
        
        plusBackground.backgroundColor = .activeElementBackground
        plusBackground.layer.cornerRadius = 40
        
        imagePlus.image = UIImage(named: "PlusNew")

        NSLayoutConstraint.activate([
            plusBackground.centerXAnchor.constraint(equalTo: frameLabel.centerXAnchor),
            plusBackground.centerYAnchor.constraint(equalTo: frameLabel.centerYAnchor),
            plusBackground.widthAnchor.constraint(equalToConstant: 80),
            plusBackground.heightAnchor.constraint(equalToConstant: 80),

            imagePlus.centerXAnchor.constraint(equalTo: frameLabel.centerXAnchor),
            imagePlus.centerYAnchor.constraint(equalTo: frameLabel.centerYAnchor),
            imagePlus.widthAnchor.constraint(equalToConstant: 40),
            imagePlus.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    @objc func continueButtonAction() {
        let emptyImage = UIImage(named: "")
        if visualOfGoal != emptyImage {
            let newVC = ForthViewController()
            navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func setup(){
        labelFirst.text = "Visualize the result"
        labelFirst.textColor = .primaryText
        labelFirst.font = .H2
        labelFirst.lineMode()
        
        label.text = "Imagine that the  dream came true. What kind of picture comes up in your head?"
        label.textColor = .gray
        label.font = .H6
        label.lineMode()
        
        frameLabel.frame = CGRect(x: 65, y: 366, width: 251, height: 298)
        frameLabel.addDashedBorder()
                
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(gestureAction(_ :) ))
        gestureRecognizer.numberOfTouchesRequired = 1
        imageItem.addGestureRecognizer(gestureRecognizer)
        imageItem.isUserInteractionEnabled = true
        imageItem.backgroundColor = .primaryBackground
        imageItem.layer.cornerRadius = 12
        imageItem.image = UIImage(named: "")
        imageItem.contentMode = .scaleAspectFill
        imageItem.clipsToBounds = true
        
        button.setBackgroundColor(.activeElementBackground, for: .normal)
        button.setBackgroundColor(.disabledElementBackground, for: .disabled)
        button.isEnabled = false
        button.layer.cornerRadius = 22
        button.setTitle("Continue", for: .normal)
        button.addTarget(self, action: #selector(continueButtonAction), for: .touchUpInside)
        button.setTitleColor(.activeElementText, for: .normal)
        button.titleLabel?.font = .A1
    }
    
    func constraints(){
        
        let buttonWidth:CGFloat = 327
        let buttonHeight:CGFloat = 48
                
        NSLayoutConstraint.activate([
            
            labelFirst.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            labelFirst.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36),
            labelFirst.topAnchor.constraint(equalTo: view.topAnchor, constant: 108),
            labelFirst.heightAnchor.constraint(equalToConstant: 48),
            labelFirst.widthAnchor.constraint(equalToConstant: 362),

            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 184),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            label.heightAnchor.constraint(equalToConstant: 83),
            label.widthAnchor.constraint(equalToConstant: 382),

            frameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 366),
            frameLabel.centerXAnchor.constraint(equalTo: fieldForImageItem.centerXAnchor),
            frameLabel.widthAnchor.constraint(equalToConstant: 251),
            frameLabel.heightAnchor.constraint(equalToConstant: 298),
            
            fieldForImageItem.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 0),
            fieldForImageItem.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            fieldForImageItem.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            fieldForImageItem.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),

            imageItem.topAnchor.constraint(equalTo: view.topAnchor, constant: 366),
            imageItem.centerXAnchor.constraint(equalTo: fieldForImageItem.centerXAnchor),
            imageItem.widthAnchor.constraint(equalToConstant: 251),
            imageItem.heightAnchor.constraint(equalToConstant: 298),
                
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            button.widthAnchor.constraint(equalToConstant:  buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight),
        ])
    }
    
    @objc func gestureAction(_ gesture: UITapGestureRecognizer){
        setImage()
    }
    
    func setImage() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    var ifImageWillChangeAgain = 0
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            plusVision = .hide
            loadView()
            viewDidLoad()
            imageItem.image = image
            visualOfGoal = image
            button.isEnabled = true
            
            if ifImageWillChangeAgain == 1 {
                deleteImage(url: ClipBoard.clipBoard.imageURL)
                ifImageWillChangeAgain = 0
            }
            
            if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                ClipBoard.clipBoard.imageURL = imgUrl.absoluteString
                ClipBoard.clipBoard.imageName = imgUrl.lastPathComponent
                savePhoto(img: image)
                ifImageWillChangeAgain = 1
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func deleteImage(url:String){
        let manager = FileManager.default
        guard let urlImage = URL(string: url) else { return }
        do {
            try manager.removeItem(at: urlImage)
        } catch {
            print(error)
        }
    }
    
    func savePhoto(img: UIImage){
        let manager = FileManager.default
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        let folderUrl = url.appendingPathComponent(folderName)
        
        let imageUrl = folderUrl.appendingPathComponent("image_\(ClipBoard.clipBoard.imageName)")

        let data: NSData = img.jpegData(compressionQuality: 1)! as NSData
        let newImage = UIImage(data: data as Data)
        let imageData = newImage?.jpegData(compressionQuality: 1.0)
        
        manager.createFile(atPath: imageUrl.path,
                           contents: imageData)
        ClipBoard.clipBoard.imageURL = "file://" + imageUrl.path

       do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
           print(folderUrl)
       } catch {
            print(error)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    enum state {
        case visible
        case hide
    }
}
