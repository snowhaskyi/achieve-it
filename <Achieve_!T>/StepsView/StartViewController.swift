//
//  ViewController.swift
//  Achieve_IT

import UIKit

final class StartViewController: UIViewController {
    
    let label = UILabel()
    let fadeLayer = UIImageView()
    let starterButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(fadeLayer)
        fadeLayer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(starterButton)
        starterButton.translatesAutoresizingMaskIntoConstraints = false
        
        constraints()
        custom()
        assignBackground()
    }
    
    @objc func continueButtonAction() {
        let newVC = FirstQuestionViewController()
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    func assignBackground(){
        let background = UIImage(named: "StartScreenBackground")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    func custom() {
        fadeLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        starterButton.backgroundColor = .activeElementBackground
        starterButton.layer.cornerRadius = 22
        starterButton.setTitle("Just do it", for: .normal)
        starterButton.addTarget(self, action: #selector(continueButtonAction), for: .touchUpInside)
        starterButton.setTitleColor(.activeElementText, for: .normal)
        starterButton.titleLabel?.font = .A1
        
        label.frame = CGRect(x: 50, y: 622, width: 338, height: 95)
        label.text = "Make your dreams come true"
        
        label.textColor = .primaryText
        label.font = .H2
        label.lineMode()
    }
    
    func constraints(){
        
        NSLayoutConstraint.activate([
            
            fadeLayer.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            fadeLayer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            fadeLayer.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            fadeLayer.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            label.bottomAnchor.constraint(equalTo: starterButton.topAnchor, constant: -80),
            
            starterButton.heightAnchor.constraint(equalToConstant: 48),
            starterButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 44),
            starterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -44),
            starterButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50)
        ])
    }
}

extension StartViewController: UIPopoverPresentationControllerDelegate {
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        .none
    }
}
