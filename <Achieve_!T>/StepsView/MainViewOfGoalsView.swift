
//  MainViewOfGoalsView.swift
//  Achieve_IT

import UIKit

final class MainViewOfGoalsView: UIViewController,
                           UICollectionViewDelegate,
                           UICollectionViewDataSource,
                                 DeleteGoal{
    
    func reloadCollectionView() {
        collectionView?.reloadData()
    }
    
    private var collectionView: UICollectionView?
    let navigationTitle = UIView()
    let nameLabel = UILabel()
    let descriptionLabel = UILabel()
    let titleHeight = CGFloat(118) // temporary
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if arrayOfGoals.isEmpty {
            restoreGoals()
        }
        
        navigationItem.hidesBackButton = true

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 171, height: 200)

        collectionView = UICollectionView(frame: .init(x: 0, y: titleHeight,
                                                       width: view.frame.width,
                                                       height: view.frame.height),
                                          collectionViewLayout: layout)
        
        guard let collectionView = collectionView else {
            return
        }
        collectionView.register(CellForMainListOfGoal.self,
                                forCellWithReuseIdentifier: CellForMainListOfGoal.identifier)
        collectionView.register(AddCellForMainListOfGoal.self,
                                forCellWithReuseIdentifier: AddCellForMainListOfGoal.identifier)

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .primaryBackground

        view.addSubview(collectionView)
        view.addSubview(navigationTitle)
        
        navigationTitle.addSubview(nameLabel)
        navigationTitle.addSubview(descriptionLabel)
        navigationTitle.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        setup()
        constraints()
    }
    
    func setup(){
        navigationTitle.backgroundColor = .primaryBackground
        
        nameLabel.text = "Your dreams and goals"
        nameLabel.font = .H5
        nameLabel.textColor = .white
        
        descriptionLabel.text = "Ambitious person aren't you?"
        descriptionLabel.textColor = .secondaryText
        descriptionLabel.font = .T2
    }
    
    func constraints(){
            NSLayoutConstraint.activate([
            navigationTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            navigationTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            navigationTitle.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            navigationTitle.heightAnchor.constraint(equalToConstant: titleHeight),
            
            nameLabel.topAnchor.constraint(equalTo: navigationTitle.topAnchor, constant: 57),
            nameLabel.leadingAnchor.constraint(equalTo: navigationTitle.leadingAnchor, constant: 15),
            nameLabel.trailingAnchor.constraint(equalTo: navigationTitle.trailingAnchor, constant: 0),
            nameLabel.heightAnchor.constraint(equalToConstant: 29),
            
            descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
            descriptionLabel.leadingAnchor.constraint(equalTo: navigationTitle.leadingAnchor, constant: 15),
            descriptionLabel.trailingAnchor.constraint(equalTo: navigationTitle.trailingAnchor, constant: 0),
            descriptionLabel.bottomAnchor.constraint(equalTo: navigationTitle.bottomAnchor, constant: 0)
        ])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfGoals.count + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AddCellForMainListOfGoal.identifier,
                                                          for: indexPath)
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.masksToBounds = true
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellForMainListOfGoal.identifier,
                                                          for: indexPath) as! CellForMainListOfGoal
            
            if let url = URL(string: arrayOfGoals[indexPath.row - 1].imageURL) {
                do {
                    let data = try Data(contentsOf: url)
                    cell.image.image = UIImage(data: data)
                } catch {
                    print("Error = ", error.localizedDescription)
                }
            }
            
            cell.label.text = arrayOfGoals[indexPath.row - 1].name
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.masksToBounds = true
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            createNewGoal()
        } else {

            let newVC = CardOfGoal()
            newVC.name.text = arrayOfGoals[indexPath.row - 1].name
            newVC.describe.text = arrayOfGoals[indexPath.row - 1].reason
            newVC.delegate = self
            indexOfGoal = indexPath.row - 1

            if let url = URL(string: arrayOfGoals[indexPath.row - 1].imageURL) {
                do {
                    let data = try Data(contentsOf: url)
                    newVC.image.image = UIImage(data: data)
                    newVC.willDoArray = arrayOfGoals[indexPath.row - 1].stepsForGoal!
                    newVC.doneArray = arrayOfGoals[indexPath.row - 1].doneArray!
                    newVC.index = indexPath.row

                } catch {
                    print("Error = ", error.localizedDescription)
                }
            }

            if let sheet = newVC.sheetPresentationController {
                sheet.detents = [.medium(), .large()]
                sheet.prefersScrollingExpandsWhenScrolledToEdge = false
                sheet.prefersGrabberVisible = true
                sheet.preferredCornerRadius = 22
            }
            present(newVC, animated: true, completion: nil)
        }
    }
            
    @objc func createNewGoal() {
        let newVC = FirstQuestionViewController()
        navigationController?.pushViewController(newVC, animated: true)
    }
 //MARK: dataMethods
    
    //fill all goals from data when the view is creating
    func restoreGoals() {
        let manager = FileManager.default
        //
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        //получили адрес файла
        let folderURL = url.appendingPathComponent(folderName)
        let fileURL = folderURL.appendingPathComponent( fileName + ".json")
        
        do {
            let data = try Data(contentsOf: fileURL)
            arrayOfGoals = try JSONDecoder().decode( [Goal].self, from: data )
        } catch {
            print(error)
        }
    }
    
    //delete Goal by index
    var indexOfGoal = 0
    
    func deleteGoal() {
        let manager = FileManager.default
        
        guard let urlImage = URL(string: arrayOfGoals[indexOfGoal].imageURL) else { return }
        do {
            try manager.removeItem(at: urlImage )
        } catch {
            print(error)
        }
        arrayOfGoals.remove(at: indexOfGoal)
        var arrayOfGoalData: Data?
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        let folderUrl = url.appendingPathComponent(folderName)
        //получить ссылку для проверки  удаляются ли фотографии или как они там хранятся
       do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
        } catch {
            print(error)
        }
        
        do {
            arrayOfGoalData = try JSONEncoder().encode(arrayOfGoals)
        } catch {
            print(error.localizedDescription)
        }

        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        print(fileUrl.path)
        manager.createFile(atPath: fileUrl.path,
                           contents: arrayOfGoalData)
    }
    
    func doChangesInGoal(index:Int, willArray:[String], doneArray:[String]) {
        let manager = FileManager.default
        let trueIndex = index - 1
        arrayOfGoals[trueIndex].doneArray = doneArray
        arrayOfGoals[trueIndex].stepsForGoal = willArray
    
        var arrayOfGoalData: Data?
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        let folderUrl = url.appendingPathComponent(folderName)
        
       do {
            try manager.createDirectory(at: folderUrl,
                                        withIntermediateDirectories: true)
        } catch {
            print(error)
        }
        
        do {
            arrayOfGoalData = try JSONEncoder().encode(arrayOfGoals)
        } catch {
            print(error.localizedDescription)
        }

        let fileUrl = folderUrl.appendingPathComponent( fileName + ".json")
        manager.createFile(atPath: fileUrl.path,
                           contents: arrayOfGoalData)
    }
}

extension MainViewOfGoalsView: UICollectionViewDelegateFlowLayout {

    // Distance Between Item Cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    // Cell Margin
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
}
