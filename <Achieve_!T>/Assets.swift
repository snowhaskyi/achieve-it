//
//  Assets.swift
//  Achieve_IT

import UIKit.UIColor
import UIKit.UIFont

extension UIColor {
    //основной
    static let primaryBackground = UIColor(red: 0.0307, green: 0.1607, blue: 0.1369, alpha: 1)
    //при вызове боттомщит и ячейки
    static let secondaryBackGround = UIColor(red: 0.1100, green: 0.2218, blue: 0.2074, alpha: 1)
    //текст при запустке экрана
    static let primaryText = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
    static let secondaryText = UIColor(red: 0.604, green: 0.604, blue: 0.604, alpha: 1)
    static let activeElementBackground = UIColor(red: 0.8137, green: 0.8519, blue: 0.5038, alpha: 1)
    static let disabledElementBackground = UIColor(red: 0.8137, green: 0.8519, blue: 0.5038, alpha: 0.5)
    //текст всего приложения
    static let activeElementText = UIColor(red: 0.149, green: 0.149, blue: 0.149, alpha: 1)
}

extension UIFont {
    static let H1 = UIFont(name: "SFProDisplay-Regular", size: 48)
    static let H2 = UIFont(name: "SFProDisplay-Regular", size: 40)
    static let H3 = UIFont(name: "SFProDisplay-Heavy", size: 32)
    static let H4 = UIFont(name: "SFProDisplay-Regular", size: 32)
    static let H5 = UIFont(name: "SFProDisplay-Heavy", size: 24)
    static let H6 = UIFont(name: "SFProDisplay-Regular", size: 23)
    static let A1 = UIFont(name: "SFProDisplay-Bold" , size: 16)
    static let T1 = UIFont(name: "SFProDisplay-Regular", size: 16)
    static let T2 = UIFont(name: "SFProDisplay-Bold", size: 14)
}
