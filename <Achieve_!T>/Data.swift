//
//  Data.swift
//  Achieve_IT

import UIKit

 class ClipBoard {
     static var clipBoard = ClipBoard()
    
     var imageURL: String
     var imageName:String
     var name: String
     var reason: String
     var stepsForGoal: [String]
     var doneArray:[String]
    
    private init(){
        imageURL = ""
        imageName = ""
        name = ""
        reason = ""
        stepsForGoal = []
        doneArray = []
    }
}


class MainNames {
    let folderName = "Source"
    let fileName = "AllGoals"
}

//Модель Цели
class Goal: Codable {
    let imageURL: String
    let name: String?
    let reason: String?
    var stepsForGoal: [String]?
    var doneArray:[String]?

    init(imageURL: String, name: String, reason: String, stepsForGoal: [String], doneArray:[String]) {
        self.imageURL = imageURL
        self.name = name
        self.reason = reason
        self.stepsForGoal = stepsForGoal
        self.doneArray = doneArray
    }
}
//Вынести в статичные установки
let folderName = "Source"
let fileName = "AllGoals"

var arrayOfGoals: [Goal] = []
var visualOfGoal = UIImage(named: "")

