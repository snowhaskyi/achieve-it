//
//  SceneDelegate.swift
//  Achieve_IT

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        restoreGoals()
        
        if let WC = scene as? UIWindowScene {
            
            let window = UIWindow(windowScene: WC)
            let navigationController = UINavigationController()
            var VC: UIViewController
            //Если нет ни одной цели, открывается стартовый экран
           
            if arrayOfGoals.isEmpty {
               let newVC = StartViewController()
                VC = newVC
            } else {
                let newVC = MainViewOfGoalsView()
                VC = newVC
            }
            
            navigationController.navigationBar.backgroundColor = .clear
            navigationController.viewControllers = [VC]
            window.rootViewController = navigationController
            
            self.window = window
            
            window.makeKeyAndVisible()
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    func restoreGoals() {
        let manager = FileManager.default
        
        guard let url = manager.urls(for: .documentDirectory,
                                     in: .userDomainMask).first else {return}
        
        let folderURL = url.appendingPathComponent(folderName)
        let fileURL = folderURL.appendingPathComponent( fileName + ".json")
        do {
            let data = try Data(contentsOf: fileURL)
            arrayOfGoals = try JSONDecoder().decode( [Goal].self, from: data )
        } catch {
            print(error)
        }
    }
    
}
