//
//  Protocols.swift
//  Achieve_IT

import UIKit
//FourViewController
//GoalCard
protocol ReloadTableViewDelegate: AnyObject {
    func reload()
}

protocol EditSheetBottomController: AnyObject {
    func callEditBottomSheet(x:Int)
}
//GoalCard
protocol ChangeStateOFinalList: AnyObject {
    func changeToWillDo()
    func changeToDone()
}
//GoalCard
protocol СhangeEditState: AnyObject {
    func editDoneButton()
    func callAlert()
}
//GoalCard
protocol EditState: AnyObject {
    func changeEditStateOfGoalCard()
}

protocol CallImagePickerDelegate: AnyObject {
    func callImagePicker()
}

protocol ReloadToDoListDelegate: AnyObject {
    func reloadList()
}

protocol DeleteGoal: AnyObject {
    func deleteGoal()
    func reloadCollectionView()
    func doChangesInGoal(index:Int, willArray:[String], doneArray:[String])
}
