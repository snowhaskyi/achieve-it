//
//  UIButton.swift
//  Achieve_IT

import UIKit
//Вероятно это лишнее расширение и его нужно удалить
extension UIButton {
    func addBottomLineWithColor(color: UIColor, width: CGFloat) {
        let bottomBorderLine = CALayer()
        bottomBorderLine.backgroundColor = color.cgColor
        bottomBorderLine.frame = CGRect(x: 0,
                                        y: self.frame.size.height - width,
                                        width: self.frame.size.width,
                                        height: width)
        self.layer.addSublayer(bottomBorderLine)
    }
}
