//
//  UIViewController.swift
//  Achieve_IT


import UIKit

extension UIViewController {

    func backBarButton() {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        button.backgroundColor = .activeElementBackground
        button.layer.cornerRadius = 22
        button.contentEdgeInsets =  UIEdgeInsets(top: 0, left: -10, bottom: 0, right: -10)
        button.setImage(UIImage(named: "BackButtonBlack"), for: .normal)
        button.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = menuBarItem
    }

    @objc func back() {
        navigationController?.popViewController(animated: true)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}
