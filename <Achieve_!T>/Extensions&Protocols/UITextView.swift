//
//  UITextView.swift
//  Achieve_IT


import UIKit

extension UITextView {

    func lineMode(){
        self.textContainer.lineBreakMode = .byWordWrapping
    }
}
